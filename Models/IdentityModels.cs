﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FinalProject.Models
{
    // You can add profile data for the user by adding more properties to your User class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required, StringLength(20)]
        public string City { get; set; }
        [Required, StringLength(2)]
        public string State { get; set; }
        [Required, Range(0, 99999)]
        public int Zip { get; set; }
        [Required, Range(0, 1000)]
        public int DistTraveled { get; set; }
        [StringLength(100)]
        public string CuisinePref { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<FinalProject.Models.ContactEmail> ContactEmails { get; set; }

        public System.Data.Entity.DbSet<FinalProject.Models.Emailer> Emailers { get; set; }

        public System.Data.Entity.DbSet<FinalProject.Models.Event> Events { get; set; }

        public System.Data.Entity.DbSet<FinalProject.Models.News> News { get; set; }       
    }
}