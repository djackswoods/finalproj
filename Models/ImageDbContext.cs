﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class ImageDbContext : DbContext
    {
        public ImageDbContext()
            : base("name=dbContext")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ImageDbContext>());
        }
        public DbSet<ImageGallery> ImageGallery { get; set; }
    }
}